package com.example.btcprice.screen.price

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import arrow.core.Either
import com.example.btcprice.model.CurrentPriceStatistics
import com.example.btcprice.repo.BitcoinRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject
import org.koin.test.mock.declareMock
import org.mockito.Mockito.`when`

class CurrentPriceViewModelTest : KoinTest {

    private val testModule = module {
        viewModel { CurrentPriceViewModel(get()) }
        single { declareMock<BitcoinRepository> { } }
        single { declareMock<LifecycleOwner> { } }
    }

    private val viewModel: CurrentPriceViewModel by inject()
    private val repository: BitcoinRepository by inject()
    private val mockLifecycleOwner: LifecycleOwner by inject()

    @Before
    fun setup() {
        startKoin { modules(testModule) }
    }

    @After
    fun teardown() {
        stopKoin()
    }

    @Test
    fun handlesSuccessfulRepositoryResponse() {
        GlobalScope.launch {
            val either = Either.right(CurrentPriceStatistics(last = 1.0))
            `when`(repository.getCurrentPriceData("")).thenReturn(either)

            viewModel.currentPriceStatistics
                .observe(mockLifecycleOwner, Observer {
                    assertEquals(it, either)
                })
        }
    }

    @Test
    fun handlesErrorRepositoryResponse() {
        GlobalScope.launch {
            val exception = Exception("Foobar")
            val either = Either.left(exception)
            `when`(repository.getCurrentPriceData("")).thenReturn(either)

            viewModel.requestError.observe(mockLifecycleOwner, Observer {
                assertEquals(it, either)
                assertEquals(it.message, exception.message)
            })
        }
    }

}