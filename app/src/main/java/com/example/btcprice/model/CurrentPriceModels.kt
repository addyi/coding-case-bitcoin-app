package com.example.btcprice.model

import com.google.gson.annotations.SerializedName

data class Open(val day: Double = 0.0)

data class Averages(val day: Double = 0.0)

data class Price(val day: Double = 0.0)

data class Percent(val day: Double = 0.0)

data class Changes(val percent: Percent, val price: Price)

data class CurrentPriceStatistics(
    val last: Double = 0.0,
    val high: Double = 0.0,
    val low: Double = 0.0,
    val open: Open = Open(),
    val averages: Averages = Averages(),
    val changes: Changes = Changes(Percent(), Price()),
    val timestamp: Long = 0,
    @SerializedName("display_symbol") val conversion: String = ""
)