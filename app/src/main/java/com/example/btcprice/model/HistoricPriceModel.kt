package com.example.btcprice.model

data class HistoricPriceDataEntry(
    val open: Double = 0.0,
    val time: String = "1970-01-01 01:00:00",
    val high: Double = 0.0,
    val low: Double = 0.0,
    val average: Double = 0.0
)