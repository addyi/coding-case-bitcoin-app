package com.example.btcprice

import android.app.Application
import androidx.preference.PreferenceManager
import com.example.btcprice.di.appModule
import com.example.btcprice.di.networkModule
import com.example.btcprice.screen.settings.put
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class BTCPriceApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        defineDefaultPreferences()

        startKoin {
            androidLogger()
            androidContext(this@BTCPriceApplication)
            modules(listOf(appModule, networkModule))
        }
    }

    private fun defineDefaultPreferences() {
        val preferences = PreferenceManager.getDefaultSharedPreferences(this)

        val conversionPreference = preferences.getString(
            getString(R.string.conversionKey),
            getString(R.string.conversionDefaultValue)
        )

        preferences.edit().let {
            it.put(Pair(getString(R.string.conversionKey), conversionPreference))
            it.apply()
        }
    }
}