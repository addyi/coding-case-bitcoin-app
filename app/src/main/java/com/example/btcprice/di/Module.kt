package com.example.btcprice.di

import com.example.btcprice.repo.BitcoinAverageService
import com.example.btcprice.repo.BitcoinRepository
import com.example.btcprice.repo.BitcoinRepositoryImpl
import com.example.btcprice.screen.history.PriceHistoryViewModel
import com.example.btcprice.screen.price.CurrentPriceViewModel
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


val appModule = module {

    single<BitcoinRepository> { BitcoinRepositoryImpl(get()) }

    viewModel { CurrentPriceViewModel(get()) }

    viewModel { PriceHistoryViewModel(get()) }

}

val networkModule = module {

    single<BitcoinAverageService> { get<Retrofit>().create(BitcoinAverageService::class.java) }

    single<Retrofit> {
        Retrofit.Builder()
            .baseUrl("https://apiv2.bitcoinaverage.com/")
            .client(get())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    single<HttpLoggingInterceptor> {
        HttpLoggingInterceptor().also {
            it.level = HttpLoggingInterceptor.Level.BODY
        }
    }

    single<OkHttpClient> {
        OkHttpClient.Builder()
            .addInterceptor(get<HttpLoggingInterceptor>())
            .build()
    }
}