package com.example.btcprice.repo

import com.example.btcprice.model.CurrentPriceStatistics
import com.example.btcprice.model.HistoricPriceDataEntry
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface BitcoinAverageService {

    @Headers("X-testing:testing")
    @GET("indices/global/ticker/{conversion}")
    suspend fun requestCurrentPrice(@Path("conversion") conversion: String): Response<CurrentPriceStatistics>

    @Headers("X-testing:testing")
    @GET("indices/global/history/{conversion}?period=monthly")
    suspend fun requestHistoricPriceData(@Path("conversion") conversion: String): Response<List<HistoricPriceDataEntry>>

}