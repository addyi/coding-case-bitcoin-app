package com.example.btcprice.repo

import android.util.Log
import arrow.core.Either
import com.example.btcprice.exceptions.ApiRequestException
import com.example.btcprice.exceptions.EmptyBodyException
import com.example.btcprice.exceptions.UnableToExecuteRequestException
import com.example.btcprice.exceptions.UnknownException
import com.example.btcprice.model.CurrentPriceStatistics
import com.example.btcprice.model.HistoricPriceDataEntry
import retrofit2.Response

interface BitcoinRepository {

    suspend fun getCurrentPriceData(conversion: String): Either<Exception, CurrentPriceStatistics>

    suspend fun getHistoricPriceData(conversion: String): Either<Exception, List<HistoricPriceDataEntry>>
}

class BitcoinRepositoryImpl(private var bitcoinAverageService: BitcoinAverageService) : BitcoinRepository {
    private val tag = BitcoinRepositoryImpl::class.java.simpleName

    override suspend fun getHistoricPriceData(conversion: String): Either<Exception, List<HistoricPriceDataEntry>> =
        try {
            val response = bitcoinAverageService.requestHistoricPriceData(conversion)
            extractBodyOrException(response)

        } catch (e: Exception) {
            Log.e(tag, "Unable to execute request ${e.message}")
            Either.left(UnableToExecuteRequestException())
        }

    override suspend fun getCurrentPriceData(conversion: String): Either<Exception, CurrentPriceStatistics> =
        try {
            val response = bitcoinAverageService.requestCurrentPrice(conversion)
            extractBodyOrException(response)
        } catch (e: Exception) {
            Log.e(tag, "Unable to execute request ${e.message}")
            Either.left(UnableToExecuteRequestException())
        }


    private fun <T> extractBodyOrException(response: Response<T>): Either<Exception, T> =
        if (response.isSuccessful) {
            val body = response.body()
            if (body != null) {
                Either.right(body)
            } else Either.left(EmptyBodyException())
        } else exceptionForResponseCode(response.code())


    private fun exceptionForResponseCode(responseCode: Int) = when (responseCode) {
        400 -> Either.left(ApiRequestException("Bad Request $responseCode"))
        401 -> Either.left(ApiRequestException("Unauthorized $responseCode"))
        402 -> Either.left(ApiRequestException("Payment Required $responseCode"))
        403 -> Either.left(ApiRequestException("Forbidden $responseCode"))
        404 -> Either.left(ApiRequestException("Not Found $responseCode"))
        418 -> Either.left(ApiRequestException("Teapots don't provide BTC prices"))
        in 300..399 -> Either.left(ApiRequestException("Redirection Problem Accrued $responseCode"))
        in 405..499 -> Either.left(ApiRequestException("Client Error Accrued $responseCode"))
        in 500..599 -> Either.left(ApiRequestException("Server Error $responseCode"))
        else -> {
            Either.left(UnknownException(responseCode.toString()))
        }
    }


}