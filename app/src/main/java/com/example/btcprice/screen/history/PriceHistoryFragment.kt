package com.example.btcprice.screen.history

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.btcprice.R
import com.google.android.material.snackbar.Snackbar
import org.koin.android.viewmodel.ext.android.viewModel


class PriceHistoryFragment : Fragment() {

    private val viewModel: PriceHistoryViewModel by viewModel()

    private lateinit var recyclerView: RecyclerView
    private lateinit var coordinatorLayout: CoordinatorLayout


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_price_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val context: Context = this.context ?: return

        viewModel.historicPriceDataEntries.observe(this, Observer { list ->
            getView()?.let {
                recyclerView = it.findViewById(R.id.priceHistoryRecyclerView)
                recyclerView.setHasFixedSize(true)
                recyclerView.layoutManager = GridLayoutManager(context, 2)
                recyclerView.adapter = HistoricDataAdapter(context, list)
            }
        })

        viewModel.requestError.observe(this, Observer { exception ->
            getView()?.let {
                coordinatorLayout = it.findViewById(R.id.historicPriceDataCoordinatorLayout)

                Snackbar.make(
                    coordinatorLayout, exception?.message ?: "Error",
                    Snackbar.LENGTH_LONG
                ).show()
            }
        })

        fetchHistoricPriceData()

    }

    private fun fetchHistoricPriceData() {
        val conversion = PreferenceManager.getDefaultSharedPreferences(this.context)
            .getString(
                getString(R.string.conversionKey),
                getString(R.string.conversionDefaultValue)
            )

        if (conversion != null) {
            viewModel.fetchHistoricPriceData(conversion = conversion)
        }
    }
}
