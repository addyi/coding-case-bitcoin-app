package com.example.btcprice.screen.settings

import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.preference.PreferenceFragmentCompat
import com.example.btcprice.R

class SettingsFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {

    private val tagLogging = SettingsFragment::class.java.simpleName

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preference_screen)

        val preferences = preferenceManager.sharedPreferences

        preferences.registerOnSharedPreferenceChangeListener(this)

        val conversionPreference = preferences.getString(
            getString(R.string.conversionKey),
            getString(R.string.conversionDefaultValue)
        )

        preferences.edit().let {
            it.put(Pair(getString(R.string.conversionKey), conversionPreference))
            it.apply()
        }
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {

        Log.d(
            tagLogging,
            "Preference changed: key=\"$key\" " +
                    "value=\"${sharedPreferences?.getString(key, "DefaultValue")}\""
        )
    }
}