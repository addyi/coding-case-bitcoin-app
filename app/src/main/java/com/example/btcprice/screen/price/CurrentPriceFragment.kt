package com.example.btcprice.screen.price

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.btcprice.R
import com.example.btcprice.model.CurrentPriceStatistics
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.DateFormat
import java.text.NumberFormat
import java.util.*

class CurrentPriceFragment : Fragment() {

    private val numberFormatter = NumberFormat.getNumberInstance()
    private val dateFormatter = DateFormat.getTimeInstance()

    private lateinit var coordinatorLayout: CoordinatorLayout
    private lateinit var refreshFab: FloatingActionButton
    private lateinit var currentPriceTextView: TextView
    private lateinit var changesTextView: TextView
    private lateinit var changesIndicator: TextView
    private lateinit var conversionTextView: TextView
    private lateinit var lastUpdateTimeTextView: TextView
    private lateinit var todayOpenPriceTextView: TextView
    private lateinit var todayHighPriceTextView: TextView
    private lateinit var todayLowPriceTextView: TextView
    private lateinit var averagePriceLast24HoursTextView: TextView

    private val viewModel: CurrentPriceViewModel by viewModel()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_current_price, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val fragmentRootView = getView() ?: view

        coordinatorLayout = fragmentRootView.findViewById(R.id.currentPriceCoordinatorLayout)
        refreshFab = fragmentRootView.findViewById(R.id.refreshFab)
        currentPriceTextView = fragmentRootView.findViewById(R.id.currentPriceTextView)
        changesTextView = fragmentRootView.findViewById(R.id.changesTextView)
        changesIndicator = fragmentRootView.findViewById(R.id.changesIndicator)
        conversionTextView = fragmentRootView.findViewById(R.id.conversionTextView)
        lastUpdateTimeTextView = fragmentRootView.findViewById(R.id.lastUpdateTimeTextView)
        todayOpenPriceTextView = fragmentRootView.findViewById(R.id.todayOpenPriceTextView)
        todayHighPriceTextView = fragmentRootView.findViewById(R.id.todayHighPriceTextView)
        todayLowPriceTextView = fragmentRootView.findViewById(R.id.todayLowPriceTextView)
        averagePriceLast24HoursTextView = fragmentRootView.findViewById(R.id.averageLast24HoursTextView)

        bindObserver()
    }

    private fun bindObserver() {
        viewModel.currentPriceStatistics.observe(
            this,
            Observer { handleNewCurrentPriceStatistics(it) })

        viewModel.requestError.observe(this, Observer {
            Snackbar.make(coordinatorLayout, it?.message ?: "Error", Snackbar.LENGTH_LONG).show()
        })

        refreshPriseStatistics()

        refreshFab.setOnClickListener { refreshPriseStatistics() }
    }

    private fun refreshPriseStatistics() {
        val conversion = PreferenceManager.getDefaultSharedPreferences(this.context)
            .getString(
                getString(R.string.conversionKey),
                getString(R.string.conversionDefaultValue)
            )

        if (conversion != null) {
            viewModel.refreshCurrentPriceStatistics(conversion = conversion)
        }
    }

    private fun handleNewCurrentPriceStatistics(currentPriceStatistics: CurrentPriceStatistics) {
        val context: Context = this.context ?: return

        val currencyFormatter = when (currentPriceStatistics.conversion) {
            "BTC-EUR" -> NumberFormat.getCurrencyInstance(Locale.GERMANY)
            "BTC-USD" -> NumberFormat.getCurrencyInstance(Locale.US)
            else -> NumberFormat.getNumberInstance()
        }

        currentPriceTextView.text = currencyFormatter.format(currentPriceStatistics.last)
        lastUpdateTimeTextView.text = dateFormatter.format(Date(currentPriceStatistics.timestamp * 1000))
        todayOpenPriceTextView.text = currencyFormatter.format(currentPriceStatistics.open.day)
        todayHighPriceTextView.text = currencyFormatter.format(currentPriceStatistics.high)
        todayLowPriceTextView.text = currencyFormatter.format(currentPriceStatistics.low)
        averagePriceLast24HoursTextView.text = currencyFormatter.format(currentPriceStatistics.averages.day)
        conversionTextView.text = currentPriceStatistics.conversion.replace("-", "/", true)

        val priceChange = currencyFormatter.format(currentPriceStatistics.changes.price.day)
        val percentChange = numberFormatter.format(currentPriceStatistics.changes.percent.day)
        changesTextView.text = resources.getString(R.string.price_change, priceChange, percentChange)

        val color: Int
        val indicator: String

        if (currentPriceStatistics.changes.price.day >= 0) {
            color = ContextCompat.getColor(context, R.color.highColor)
            indicator = getString(R.string.high)
        } else {
            color = ContextCompat.getColor(context, R.color.lowColor)
            indicator = getString(R.string.low)
        }
        changesIndicator.text = indicator
        changesTextView.setTextColor(color)
        changesIndicator.setTextColor(color)

    }
}
