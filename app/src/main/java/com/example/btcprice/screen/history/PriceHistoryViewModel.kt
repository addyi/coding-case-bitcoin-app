package com.example.btcprice.screen.history

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import arrow.core.Either
import com.example.btcprice.model.HistoricPriceDataEntry
import com.example.btcprice.repo.BitcoinRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

class PriceHistoryViewModel(private val repo: BitcoinRepository) : ViewModel() {

    private val tag = PriceHistoryViewModel::class.java.simpleName

    val historicPriceDataEntries: MutableLiveData<List<HistoricPriceDataEntry>> = MutableLiveData()
    val requestError: MutableLiveData<Exception> = MutableLiveData()

    private val viewModelJob = SupervisorJob()
    private val viewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    fun fetchHistoricPriceData(conversion: String) {
        viewModelScope.launch {
            when (val eitherSuccessOrFailure = repo.getHistoricPriceData(conversion)) {
                is Either.Left -> {
                    requestError.postValue(eitherSuccessOrFailure.a)
                }
                is Either.Right -> {
                    historicPriceDataEntries.postValue(eitherSuccessOrFailure.b)
                }
            }
        }
    }
}