package com.example.btcprice.screen.price

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import arrow.core.Either
import com.example.btcprice.model.CurrentPriceStatistics
import com.example.btcprice.repo.BitcoinRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

class CurrentPriceViewModel(private val repo: BitcoinRepository) : ViewModel() {

    private val tag = CurrentPriceViewModel::class.java.simpleName
    val currentPriceStatistics: MutableLiveData<CurrentPriceStatistics> = MutableLiveData()
    val requestError: MutableLiveData<Exception> = MutableLiveData()

    private val viewModelJob = SupervisorJob()
    private val viewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    fun refreshCurrentPriceStatistics(conversion: String) {
        viewModelScope.launch {
            when (val eitherSuccessOrFailure = repo.getCurrentPriceData(conversion)) {
                is Either.Left -> {
                    requestError.postValue(eitherSuccessOrFailure.a)
                }
                is Either.Right -> {
                    currentPriceStatistics.postValue(eitherSuccessOrFailure.b)
                }
            }
        }
    }

    override fun onCleared() {
        viewModelJob.cancel()
        super.onCleared()
    }
}