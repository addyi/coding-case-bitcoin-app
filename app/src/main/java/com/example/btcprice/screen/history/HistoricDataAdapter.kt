package com.example.btcprice.screen.history

import android.content.Context
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.btcprice.R
import com.example.btcprice.model.HistoricPriceDataEntry
import java.text.DateFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*


class HistoricDataAdapter(
    private val context: Context,
    private val historicPriceDataEntries: List<HistoricPriceDataEntry>
) : RecyclerView.Adapter<HistoricDataAdapter.HistoricDataItemViewHolder>() {

    private val dateFormatter = DateFormat.getDateInstance(DateFormat.MEDIUM)
    private val timeFormatter = DateFormat.getTimeInstance(DateFormat.SHORT)
    private val dateParser = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoricDataItemViewHolder =
        HistoricDataItemViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.history_entry_card,
                parent,
                false
            )
        )

    override fun getItemCount(): Int = historicPriceDataEntries.size

    override fun onBindViewHolder(holder: HistoricDataItemViewHolder, position: Int) {

        val priceDataEntry = historicPriceDataEntries[position]

        val conversion = PreferenceManager.getDefaultSharedPreferences(context).getString(
            context.getString(R.string.conversionKey),
            context.getString(R.string.conversionDefaultValue)
        )

        val currencyFormatter = when (conversion) {
            "BTCEUR" -> NumberFormat.getCurrencyInstance(Locale.GERMANY)
            "BTCUSD" -> NumberFormat.getCurrencyInstance(Locale.US)
            else -> NumberFormat.getNumberInstance()
        }

        val date = dateParser.parse(priceDataEntry.time)
        holder.historicDataDateTextView.text = dateFormatter.format(date)
        holder.historicDataTimeTextView.text = timeFormatter.format(date)
        holder.historicDataHighTextView.text = currencyFormatter.format(priceDataEntry.high)
        holder.historicDataAverageTextView.text = currencyFormatter.format(priceDataEntry.average)
        holder.historicDataLowTextView.text = currencyFormatter.format(priceDataEntry.low)

    }

    class HistoricDataItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var historicDataDateTextView: TextView = itemView.findViewById(R.id.historicDataDate)
        var historicDataTimeTextView: TextView = itemView.findViewById(R.id.historicDataTime)
        var historicDataHighTextView: TextView = itemView.findViewById(R.id.historicDataHigh)
        var historicDataAverageTextView: TextView = itemView.findViewById(R.id.historicDataAverage)
        var historicDataLowTextView: TextView = itemView.findViewById(R.id.historicDataLow)
    }

}
