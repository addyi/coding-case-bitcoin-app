package com.example.btcprice.exceptions

class EmptyBodyException : Exception("EmptyBodyException")

class ApiRequestException(message: String) : Exception(message)

class UnknownException(additionalInfo: String = "") : Exception("Unknown Error $additionalInfo")

class UnableToExecuteRequestException() : Exception("Unable to execute request")