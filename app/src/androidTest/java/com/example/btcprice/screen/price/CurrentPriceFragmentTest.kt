package com.example.btcprice.screen.price

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.example.btcprice.R
import com.example.btcprice.screen.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class CurrentPriceFragmentTest {

    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun areLabelsCorrectlyDisplayed() {
        onView(withId(R.id.conversionTextView)).check(matches(withText(R.string.btc_eur)))
        onView(withId(R.id.lastUpdateTimeLabel)).check(matches(withText(R.string.last_update)))
        onView(withId(R.id.todayOpenPriceLabel)).check(matches(withText(R.string.today_open)))
        onView(withId(R.id.todayHighPriceLabel)).check(matches(withText(R.string.today_high)))
        onView(withId(R.id.todayLowPriceLabel)).check(matches(withText(R.string.today_low)))
        onView(withId(R.id.averageLast24HoursLabel)).check(matches(withText(R.string.average_24h)))
    }
}